#[macro_use]
extern crate log;
extern crate log4rs;

use async_recursion::async_recursion;
use reqwest::header::HeaderMap;
use serde_json::Value;
use std::collections::HashMap;
use std::io;
use tokio::join;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    log4rs::init_file("config/log.yaml", Default::default()).unwrap();

    let mut addr_break_flag: bool = true;
    while addr_break_flag {
        let mut addr = "http://127.0.0.1:8081".to_string();
        info!("--请输入nexus服务访问地址(默认：{})", addr);
        let mut addr_input = String::new();
        let addr_read_result = io::stdin().read_line(&mut addr_input);
        match addr_read_result {
            Ok(_) => {
                let _addr_input = addr_input.trim().to_string();
                if !_addr_input.is_empty() {
                    if _addr_input.ends_with("/") {
                        addr = _addr_input[.._addr_input.len() - 1].to_string();
                    } else {
                        addr = _addr_input;
                    }
                }
                addr_break_flag = false;
                let mut uri_break_flag: bool = true;
                while uri_break_flag {
                    let mut uri = "/".to_string();
                    info!("请输入需要下载的目录信息(默认：{})", uri);
                    let mut uri_input = String::new();
                    let uri_read_result = io::stdin().read_line(&mut uri_input);
                    match uri_read_result {
                        Ok(_) => {
                            let _uri_input = uri_input.trim().to_string();
                            if !_uri_input.is_empty() {
                                uri = _uri_input;
                                if !uri.ends_with("/") {
                                    uri = format!("{}/", &uri);
                                }
                                if !uri.starts_with("/") {
                                    uri = format!("/{}", &uri);
                                }
                            }
                            uri_break_flag = false;
                            info!("同步nexus2资源任务启动...");
                            let base_url = &format!(
                                "{}{}",
                                addr, "/nexus/service/local/repositories/central/"
                            );
                            let index_content = "index_content";
                            let content = "content";
                            let base_index_url = &format!("{}{}", base_url, index_content);
                            let base_content_url = &format!("{}{}", base_url, content);
                            let result = handle(&uri, base_index_url, base_content_url).await;
                            match result {
                                Ok(_res) => {
                                    info!("任务执行完毕");
                                }
                                Err(_e) => {}
                            }
                        }
                        Err(_e) => {}
                    }
                }
            }
            Err(_e) => {
                error!("输入有误，请重新输入")
            }
        }
    }
    Ok(())
}

#[async_recursion]
async fn handle(
    uri: &str,
    base_index_url: &str,
    base_content_url: &str,
) -> Result<(), reqwest::Error> {
    let request_url = &format!("{}{}", base_index_url, uri);
    let res = get_defalut_headers(request_url).await;
    Ok(match res {
        Ok(_res) => {
            handle_data(_res.get("data").unwrap(), base_index_url, base_content_url).await?
        }
        Err(_e) => {
            error!("请求失败,url:{}", request_url);
        }
    })
}

#[async_recursion]
async fn handle_data(
    data: &serde_json::Value,
    base_index_url: &str,
    base_content_url: &str,
) -> Result<(), reqwest::Error> {
    // 数据类型
    // G:group A:artifact V:value
    let value_type = data.get("type").unwrap().as_str().unwrap();
    if "G".eq(value_type) {
        let g_childrens = data.get("children");
        match g_childrens {
            Some(_g_childrens) => {
                let _g_childrens_array = _g_childrens.as_array().unwrap();
                // 需要遍历
                Ok(for _g_item in _g_childrens_array {
                    let result = handle_data(_g_item, base_index_url, base_content_url).await;
                    match result {
                        Ok(_item) => {}
                        Err(_e) => {
                            error!("group遍历失败,grep:{},error:{}", _g_item, _e);
                        }
                    }
                })
            }
            None => {
                // 需要再向下请求
                Ok(handle(
                    data.get("path").unwrap().as_str().unwrap(),
                    base_index_url,
                    base_content_url,
                )
                .await?)
            }
        }
    } else if "A".eq(value_type) {
        // 遍历artifactId的children
        let artifact_childrens = data.get("children").unwrap().as_array().unwrap();
        Ok(for a_item in artifact_childrens {
            let result = handle_data(a_item, base_index_url, base_content_url).await;
            match result {
                Ok(_item) => {}
                Err(_e) => {
                    error!("artifact遍历失败,grep:{},error:{}", a_item, _e);
                }
            }
        })
    } else {
        let v_childrens = data.get("children").unwrap();
        let v_array = v_childrens.as_array().unwrap();
        // 查看是否存在'
        // let content_url = &format!(
        //     "{}{}",
        //     base_content_url,
        //     data.get("path").unwrap().as_str().unwrap()
        // );
        // let content_result = get_defalut_headers(content_url).await;
        // let mut exist_content_path: Vec<&str> = Vec::new();
        // match content_result {
        //     Ok(_content_result) => {
        //         // 去除已存在的包
        //         let content_array = _content_result.get("data").unwrap().as_array().unwrap();
        //         for content_item in content_array {
        //             let pt = content_item
        //                 .get("relativePath")
        //                 .unwrap()
        //                 .as_str()
        //                 .unwrap()
        //                 .clone();
        //             exist_content_path.push(pt);
        //         }
        //     }
        //     Err(_e) => {
        //         error!(
        //             "请求content失败，http状态码:[{}],url:[{}]",
        //             _e.status().unwrap(),
        //             content_url
        //         )
        //     }
        // }

        Ok(for v_item in v_array {
            let v_item_path = data.get("path").unwrap().as_str().unwrap();
            // if exist_content_path.contains(&v_item_path) {}
            // 触发下载
            let artifact_uri_download = download(
                v_item.get("artifactUri").unwrap().as_str().unwrap(),
                v_item_path,
            );
            let pom_uri_download =
                download(v_item.get("pomUri").unwrap().as_str().unwrap(), v_item_path);
            let _ = join!(artifact_uri_download, pom_uri_download);
        })
    }
}

async fn download(url: &str, path: &str) -> Result<(), reqwest::Error> {
    if url != "" {
        let client = reqwest::Client::new();
        let status = client.get(url).send().await?;
        match status.error_for_status() {
            Ok(_res) => {
                info!("下载文件成功,url:[{}]", url);
            }
            Err(_e) => {
                error!(
                    "下载文件失败,path:[{}],http状态码:[{}],url:[{}]",
                    path,
                    _e.status().unwrap(),
                    url
                );
            }
        }
    }
    Ok(())
}

async fn get(url: &str, headers: HeaderMap) -> Result<HashMap<String, Value>, reqwest::Error> {
    let client = reqwest::Client::new();
    Ok(client
        .get(url)
        .headers(headers)
        .send()
        .await?
        .json::<HashMap<String, Value>>()
        .await?)
}

async fn get_defalut_headers(url: &str) -> Result<HashMap<String, Value>, reqwest::Error> {
    let mut headers = HeaderMap::new();
    headers.insert("Content-Type", "application/json".parse().unwrap());
    headers.insert("accept", "application/json,application/vnd.siesta-error-v1+json,application/vnd.siesta-validation-errors-v1+json".parse().unwrap());
    get(url, headers).await
}
