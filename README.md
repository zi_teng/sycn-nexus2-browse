# sycn-nexus2-browse

## 系统介绍

- sycn-nexus2-browse 是一个使用rust语言开发的脚本工程，目的：同步maven中央仓库资源到nexus2私服仓库。

  > 应用场景：nexus2部署在隔离环境无法访问互联网，则需要外部可访问互联网的nexus2系统进行资源同步，之后将同步的资源拷贝入隔离环境完成同步

## Cargo依赖

```
reqwest = { version = "0.11", features = ["json"] }
tokio = { version = "1", features = ["full"] }
serde_json = "1.0.64"
log = "0.4.14"
log4rs = "1.0.0"
async-recursion = "0.3.2"
```

## 使用

### 使用方法一（稳定）：

- 在源码根目录执行`cargo build --release`,之后会在`target/release`目录下生成`sync-nexus2-browse`脚本(windows环境为`sync-nexus2-browse.exe`)，启动脚本即可
  
  - 注意点：脚本需要同级目录下有一个`config/log.yaml`文件，目的是为了配置脚本打印的日志信息

### 使用方法二（方便）：

- 在源码根目录下执行`cargo run`（启动项目），按照提示操作即可。

## 规划及迭代

### 规划内容

- [ ] 多线程并发处理，根据不同`group`信息启动单独的线程处理
- [ ] 动态比对下载内容本地是否存在，已存在内容不触发下载

### v 0.2.0 - 2021-06-29

  * 动态输入nexus2访问地址，一个编译脚本可对接多个nexus2仓库
  * 根据`group`分组信息下载相对应的依赖，支持动态输入

### v 0.1.0 - 2021-06-28

  * 对接nexus2的index索引api
  * 解析索引api返回信息，触发下载动作，进行全量下载

